<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class CategoryTreeController extends AbstractController
{
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }
    public function __invoke()
    {

        return $this->categoryRepository->findBy(["parent"=> null]);
    }
}

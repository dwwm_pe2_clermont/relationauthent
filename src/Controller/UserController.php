<?php

namespace App\Controller;

use ApiPlatform\Validator\Exception\ValidationException;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
class UserController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $hasher){
        $this->passwordEncoder = $hasher;
    }
    public function __invoke(User $user)
    {
        if($user->getPassword() !== $user->getConfirmPassword()){
            throw new ValidationException(
                "Les mots de passes ne sont pas identiques");
        }
        $user->setPassword($this->passwordEncoder->hashPassword($user, $user->getPassword()));
        return $user;
    }
}

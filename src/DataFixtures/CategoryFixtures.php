<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categoty = new Category();
        $categoty->setName("Pantalon");
        $categoty->setImage("https://cdn-icons-png.flaticon.com/512/1250/1250119.png");

        $category2 = new Category();
        $category2->setName("Jeans");
        $category2->setImage("https://cdn-icons-png.flaticon.com/512/1176/1176990.png");
        $category2->setParent($categoty);

        $category3 = new Category();
        $category3->setName("Short Jeans");
        $category3->setImage("https://cdn-icons-png.flaticon.com/512/88/88783.png");
        $category3->setParent($category2);

        $category4 = new Category();
        $category4->setName("Chaussures");
        $category4->setImage("https://cdn-icons-png.flaticon.com/512/919/919275.png");

        $category5 = new Category();
        $category5->setName("Basket");
        $category5->setImage('https://static.vecteezy.com/ti/vecteur-libre/p1/14865191-vecteur-de-contour-d-icone-de-conception-de-baskets-chaussure-de-sport-vectoriel.jpg');
        $category5->setParent($category4);

        $manager->persist($categoty);
        $manager->persist($category2);
        $manager->persist($category3);
        $manager->persist($category4);
        $manager->persist($category5);

        $manager->flush();
    }
}
